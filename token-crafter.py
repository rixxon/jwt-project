import jwt
from datetime import datetime, timedelta
import os


def craft():
    id = input("Insert the id: ")
    key = input("Insert the secret key: ")
    token = jwt.encode(
        {
            "public_id": id,
            "admin": True,
            "exp": datetime.utcnow() + timedelta(minutes=30),
        },
        str(key),
        algorithm="HS256",
    )
    print(token)


if __name__ == "__main__":
    craft()
