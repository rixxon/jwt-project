import requests
import getpass


class Connector:
    token = None

    def __init__(self, url):
        self.url = url

    def show_token(self):
        print(self.token)

    def fetch(self):
        headers = {
            'x-access-tokens': self.token
        }
        response = requests.get(self.url + "/users", headers=headers)
        return response.json()

    def login(self, email, password):
        data = {
            "email": email,
            "password": password
        }
        response = requests.post(self.url + "/login", json=data)
        try:
            self.token = response.json()['token']
        except:
            self.token = None
        if self.token:
            print("Login successful")
        else:
            print("Login failed")


def main():
    connector = Connector("http://localhost:5000")
    while True:
        print("1. Login")
        print("2. Fetch")
        print("3. Show token")
        print("4. Exit")
        choice = int(input("Enter your choice: "))
        if choice == 1:
            email = input("Enter your email: ")
            password = getpass.getpass("Enter your password: ")
            connector.login(email, password)
        if choice == 2:
            print(connector.fetch())
        if choice == 3:
            connector.show_token()
        if choice == 4:
            break


if __name__ == '__main__':
    main()
