# flask imports
from flask import Flask, request, jsonify, make_response
from flask_sqlalchemy import SQLAlchemy
import uuid  # for public id
from werkzeug.security import generate_password_hash, check_password_hash

# imports for PyJWT authentication
import jwt
from datetime import datetime, timedelta
from functools import wraps
from flask import request
from sqlalchemy.sql import func
import os

basedir = os.path.abspath(os.path.dirname(__file__))

app = Flask(__name__)
app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///" + os.path.join(
    basedir, "database.db"
)
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
app.config["SECRET_KEY"] = os.environ.get("SECRET_KEY")
print("The secret is: " + str(app.config["SECRET_KEY"]))

db = SQLAlchemy(app)


class Users(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    public_id = db.Column(db.Integer)
    name = db.Column(db.String(50))
    password = db.Column(db.String(50))
    email = db.Column(db.String(50))
    admin = db.Column(db.Boolean)


class Books(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey("users.id"), nullable=False)
    name = db.Column(db.String(50), unique=True, nullable=False)
    Author = db.Column(db.String(50), unique=True, nullable=False)
    Publisher = db.Column(db.String(50), nullable=False)
    book_prize = db.Column(db.Integer)


def token_required(f):
    @wraps(f)
    def decorator(*args, **kwargs):
        token = None
        if "x-access-tokens" in request.headers:
            token = request.headers["x-access-tokens"]

        if not token:
            return jsonify({"message": "a valid token is missing"})
        try:
            data = jwt.decode(
                token, str(app.config["SECRET_KEY"]), algorithms=["HS256"]
            )
            current_user = Users.query.filter_by(public_id=data["public_id"]).first()
        except:
            return jsonify({"message": "token is invalid"})

        return f(current_user, *args, **kwargs)

    return decorator


@app.route("/register", methods=["POST"])
def signup_user():
    data = request.json
    data["email"] = data["email"].lower()
    user = Users.query.filter_by(email=data["email"]).first()
    if user:
        return jsonify({"message": "email already exists"})
    hashed_password = generate_password_hash(data["password"], method="sha256")
    email = data["email"]
    new_user = Users(
        public_id=str(uuid.uuid4()),
        name=data["username"],
        password=hashed_password,
        email=email.lower(),
        admin=False,
    )
    db.session.add(new_user)
    db.session.commit()
    return jsonify({"message": "registered successfully"})


@app.route("/login", methods=["POST"])
def login_user():
    auth = request.json
    if not auth or not auth["email"] or not auth["password"]:
        return make_response(
            "could not verify", 401, {"Authentication": 'login required"'}
        )

    user = Users.query.filter_by(email=auth["email"].lower()).first()
    if not user:
        return make_response(
            "could not verify", 401, {"Authentication": 'login required"'}
        )
    if check_password_hash(user.password, auth["password"]):
        token = jwt.encode(
            {
                "public_id": user.public_id,
                "exp": datetime.utcnow() + timedelta(minutes=30),
            },
            str(app.config["SECRET_KEY"]),
            algorithm="HS256",
        )
        return jsonify({"token": token})

    return make_response(
        "could not verify", 401, {"Authentication": '"login required"'}
    )


@app.route("/users", methods=["GET"])
@token_required
def get_all_users(current_user):
    if not current_user.admin:
        return jsonify({"message": "cannot perform that function"})
    users = Users.query.all()
    result = []
    for user in users:
        user_data = {}
        user_data["public_id"] = user.public_id
        user_data["name"] = user.name
        user_data["password"] = user.password
        user_data["admin"] = user.admin
        user_data["email"] = user.email

        result.append(user_data)
    return jsonify({"users": result})


@app.route("/books", methods=["POST"])
@token_required
def create_book(current_user):
    data = request.get_json()

    new_books = Books(
        name=data["name"],
        Author=data["Author"],
        Publisher=data["Publisher"],
        book_prize=data["book_prize"],
        user_id=current_user.id,
    )
    db.session.add(new_books)
    db.session.commit()
    return jsonify({"message": "new books created"})


@app.route("/books", methods=["GET"])
@token_required
def get_books(current_user):
    books = Books.query.filter_by(user_id=current_user.id).all()
    output = []
    for book in books:
        book_data = {}
        book_data["id"] = book.id
        book_data["name"] = book.name
        book_data["Author"] = book.Author
        book_data["Publisher"] = book.Publisher
        book_data["book_prize"] = book.book_prize
        output.append(book_data)

    return jsonify({"list_of_books": output})


@app.route("/books/<book_id>", methods=["DELETE"])
@token_required
def delete_book(current_user, book_id):
    book = Books.query.filter_by(id=book_id, user_id=current_user.id).first()
    if not book:
        return jsonify({"message": "book does not exist"})

    db.session.delete(book)
    db.session.commit()
    return jsonify({"message": "Book deleted"})


if __name__ == "__main__":
    app.run(debug=True)
